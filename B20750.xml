<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B20750">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="_245">The Country parson's honest advice to that judicious lawyer and worthy minister of state my Lord Keeper</title>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B17162 of text T146834 in the <ref target="B20750-http;//estc.bl.uk">English Short Title Catalog</ref>. Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in	 a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2KB of XML-encoded text transcribed from 2 1-bit group-IV TIFF page images.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B20750</idno>
    <idno type="stc">Wing C6568</idno>
    <idno type="oclc">ocm 11196698</idno>
    <availability>
     <p>This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. Searching, reading, printing, or downloading EEBO-TCP texts is reserved for the authorized users of these project partner institutions. Permission must be granted for subsequent distribution, in print or electronically, of this text, in whole or in part. Please contact project staff at eebotcp-info(at)umich.edu for further information or permissions.</p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online text creation partnership.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 2, no. B20750)</note>
    <note>Transcribed from: (Early English Books Online ; image set 204877)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1438:39)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title type="_245">The Country parson's honest advice to that judicious lawyer and worthy minister of state my Lord Keeper</title>
      <author>Browne, Joseph, fl. 1706.</author>
      <author>Swift, Jonathan, 1667-1745.</author>
     </titleStmt>
     <extent>1 sheet [2 p.]   </extent>
     <publicationStmt>
      <publisher>s.n.,</publisher>
      <pubPlace>[London :</pubPlace>
      <date>1700?]</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Variously attributed to Joseph Browne and Jonathan Swift.</note>
      <note>Imprint suggested by Wing, NUC pre-1956 imprints.</note>
      <note>Imperfect: print show-through.</note>
      <note>In verse.</note>
      <note>My Lord Keeper is William Cowper.</note>
      <note>Reproduction of the original in the Harvard University Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc>
    <p>Header created with script mrcb2eeboutf.xsl on 2016-01-28.</p>
   </projectDesc>
   <editorialDecl n="4">
    <p>Manually keyed and coded text linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
    <p>Issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
    <p>Keying and markup guidelines available at TCP web site (http://www.textcreationpartnership.org/docs/)</p>
   </editorialDecl>
  </encodingDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="lcsh">
     <term type="personal_name">Cowper, William Cowper, --  Earl of, ca. 1665-1723 --  Anecdotes.</term>
     <term type="geographic_name">Great Britain --  Politics and government --  1689-1702 --  Anecdotes</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>The Country parson's honest advice to that judicious lawyer and worthy minister of state my Lord Keeper</ep:title>
    <ep:author>n/a</ep:author>
    <ep:publicationYear>1700</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>2</ep:pageCount>
    <ep:wordCount>177</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2014-02 </date>
    <label>SPi Global</label>Keyed and coded from ProQuest page images 
      </change>
   <change>
    <date>2014-09 </date>
    <label>Simon Charles </label>Sampled and proofread 
      </change>
   <change>
    <date>2014-09 </date>
    <label>Simon Charles</label>Text and markup reviewed and edited 
      </change>
   <change>
    <date>2015-03 </date>
    <label>pfs</label>Batch review (QC) and XML conversion 
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B20750-e10010" xml:lang="eng">
  <body xml:id="B20750-e10020">
   <div type="advice" xml:id="B20750-e10030">
    <pb facs="1" n="1" xml:id="B20750-001-a"/>
    <head xml:id="B20750-e10040">
     <w lemma="the" pos="d" xml:id="B20750-001-a-0010">THE</w>
     <hi xml:id="B20750-e10050">
      <w lemma="country" pos="n1" xml:id="B20750-001-a-0020">Country</w>
      <w lemma="parson" pos="ng1" xml:id="B20750-001-a-0030">Parson's</w>
      <w lemma="honest" pos="j" xml:id="B20750-001-a-0040">Honest</w>
      <w lemma="advice" pos="n1" xml:id="B20750-001-a-0050">Advice</w>
     </hi>
     <w lemma="to" pos="acp" xml:id="B20750-001-a-0060">TO</w>
     <w lemma="that" pos="cs" xml:id="B20750-001-a-0070">THAT</w>
     <w lemma="judicious" pos="j" xml:id="B20750-001-a-0080">Judicious</w>
     <w lemma="lawyer" pos="n1" xml:id="B20750-001-a-0090">Lawyer</w>
     <pc xml:id="B20750-001-a-0100">,</pc>
     <w lemma="and" pos="cc" xml:id="B20750-001-a-0110">AND</w>
     <w lemma="worthy" pos="jnn" xml:id="B20750-001-a-0120">Worthy</w>
     <w lemma="minister" pos="n1" xml:id="B20750-001-a-0130">Minister</w>
     <w lemma="of" pos="acp" xml:id="B20750-001-a-0140">of</w>
     <w lemma="state" pos="n1" xml:id="B20750-001-a-0150">State</w>
     <pc xml:id="B20750-001-a-0160">,</pc>
     <w lemma="my" pos="po" xml:id="B20750-001-a-0170">My</w>
     <w lemma="lord" pos="n1" xml:id="B20750-001-a-0180">Lord</w>
     <w lemma="keeper" pos="n1" xml:id="B20750-001-a-0190">Keeper</w>
     <pc unit="sentence" xml:id="B20750-001-a-0200">.</pc>
    </head>
    <lg xml:id="B20750-e10060">
     <l xml:id="B20750-e10070">
      <w lemma="be" pos="vvb" xml:id="B20750-001-a-0210">BE</w>
      <w lemma="wise" pos="j" xml:id="B20750-001-a-0220">Wise</w>
      <w lemma="as" pos="acp" xml:id="B20750-001-a-0230">as</w>
      <w lemma="somerset" pos="nn1" rend="hi" xml:id="B20750-001-a-0240">Somerset</w>
      <pc xml:id="B20750-001-a-0250">,</pc>
      <w lemma="as" pos="acp" xml:id="B20750-001-a-0260">as</w>
      <w lemma="Somer" pos="nng1" rend="hi-apo-plain" xml:id="B20750-001-a-0280">Somer's</w>
      <w lemma="brave" pos="j" xml:id="B20750-001-a-0290">Brave</w>
      <pc xml:id="B20750-001-a-0300">,</pc>
     </l>
     <l xml:id="B20750-e10080">
      <w lemma="as" pos="acp" xml:id="B20750-001-a-0310">As</w>
      <w lemma="pembroke" pos="nn1" rend="hi" xml:id="B20750-001-a-0320">Pembroke</w>
      <w lemma="airy" pos="j" xml:id="B20750-001-a-0330">Airy</w>
      <pc xml:id="B20750-001-a-0340">,</pc>
      <w lemma="and" pos="cc" xml:id="B20750-001-a-0350">and</w>
      <w lemma="as" pos="acp" xml:id="B20750-001-a-0360">as</w>
      <w lemma="richmond" pos="nn1" rend="hi" xml:id="B20750-001-a-0370">Richmond</w>
      <w lemma="grave" pos="j" xml:id="B20750-001-a-0380">Grave</w>
      <pc xml:id="B20750-001-a-0390">;</pc>
     </l>
     <l xml:id="B20750-e10090">
      <w lemma="humble" pos="j" xml:id="B20750-001-a-0400">Humble</w>
      <w lemma="as" pos="acp" xml:id="B20750-001-a-0410">as</w>
      <w lemma="orford" pos="nn1" rend="hi" xml:id="B20750-001-a-0420">Orford</w>
      <w lemma="be" pos="vvb" xml:id="B20750-001-a-0430">be</w>
      <pc xml:id="B20750-001-a-0440">;</pc>
      <w lemma="and" pos="cc" xml:id="B20750-001-a-0450">and</w>
      <w lemma="Wharton" pos="nng1" rend="hi-apo-plain" xml:id="B20750-001-a-0470">Wharton's</w>
      <w lemma="zeal" pos="n1" xml:id="B20750-001-a-0480">Zeal</w>
      <pc xml:id="B20750-001-a-0490">,</pc>
     </l>
     <l xml:id="B20750-e10100">
      <w lemma="for" pos="acp" xml:id="B20750-001-a-0500">For</w>
      <w lemma="church" pos="n1" xml:id="B20750-001-a-0510">Church</w>
      <w lemma="and" pos="cc" xml:id="B20750-001-a-0520">and</w>
      <w lemma="loyalty" pos="n1" xml:id="B20750-001-a-0530">Loyalty</w>
      <pc xml:id="B20750-001-a-0540">,</pc>
      <w lemma="will" pos="vmd" reg="would" xml:id="B20750-001-a-0550">wou'd</w>
      <w lemma="fit" pos="vvi" xml:id="B20750-001-a-0560">fit</w>
      <w lemma="thou" pos="png" xml:id="B20750-001-a-0570">thee</w>
      <w lemma="well" pos="av" xml:id="B20750-001-a-0580">well</w>
      <pc xml:id="B20750-001-a-0590">;</pc>
     </l>
     <l xml:id="B20750-e10110">
      <w lemma="like" pos="j" xml:id="B20750-001-a-0600">Like</w>
      <w lemma="sarum" pos="nn1" rend="hi" xml:id="B20750-001-a-0610">Sarum</w>
      <pc xml:id="B20750-001-a-0620">,</pc>
      <w lemma="i" pos="pns" xml:id="B20750-001-a-0630">I</w>
      <w lemma="will" pos="vmd" reg="would" xml:id="B20750-001-a-0640">wou'd</w>
      <w lemma="have" pos="vvi" xml:id="B20750-001-a-0650">have</w>
      <w lemma="thou" pos="png" xml:id="B20750-001-a-0660">thee</w>
      <w lemma="love" pos="vvb" xml:id="B20750-001-a-0670">love</w>
      <w lemma="the" pos="d" xml:id="B20750-001-a-0680">the</w>
      <w lemma="church" pos="n1" xml:id="B20750-001-a-0690">Church</w>
      <pc xml:id="B20750-001-a-0700">,</pc>
     </l>
     <l xml:id="B20750-e10120">
      <w lemma="he" pos="pns" xml:id="B20750-001-a-0710">He</w>
      <w lemma="scorn" pos="vvz" xml:id="B20750-001-a-0720">scorns</w>
      <w lemma="to" pos="acp" xml:id="B20750-001-a-0730">to</w>
      <w lemma="leave" pos="vvi" xml:id="B20750-001-a-0740">leave</w>
      <w lemma="his" pos="po" xml:id="B20750-001-a-0750">his</w>
      <w lemma="mother" pos="n1" xml:id="B20750-001-a-0760">Mother</w>
      <w lemma="in" pos="acp" xml:id="B20750-001-a-0770">in</w>
      <w lemma="the" pos="d" xml:id="B20750-001-a-0780">the</w>
      <w lemma="lurch" pos="vvi" xml:id="B20750-001-a-0790">Lurch</w>
      <pc unit="sentence" xml:id="B20750-001-a-0800">.</pc>
     </l>
     <l xml:id="B20750-e10130">
      <w lemma="for" pos="acp" xml:id="B20750-001-a-0810">For</w>
      <w lemma="the" pos="d" xml:id="B20750-001-a-0820">the</w>
      <w lemma="well" pos="av" xml:id="B20750-001-a-0830">well</w>
      <w lemma="govern" pos="vvg" xml:id="B20750-001-a-0840">governing</w>
      <w lemma="your" pos="po" xml:id="B20750-001-a-0850">your</w>
      <w lemma="family" pos="n1" xml:id="B20750-001-a-0860">Family</w>
      <pc xml:id="B20750-001-a-0870">,</pc>
     </l>
     <l xml:id="B20750-e10140">
      <w lemma="let" pos="vvb" xml:id="B20750-001-a-0880">Let</w>
      <w lemma="pious" pos="j" xml:id="B20750-001-a-0890">pious</w>
      <w lemma="haversham" pos="nn1" rend="hi" xml:id="B20750-001-a-0900">Haversham</w>
      <w lemma="thy" pos="po" xml:id="B20750-001-a-0910">thy</w>
      <w lemma="pattern" pos="n1" xml:id="B20750-001-a-0920">Pattern</w>
      <w lemma="be" pos="vvi" xml:id="B20750-001-a-0930">be</w>
      <pc xml:id="B20750-001-a-0940">:</pc>
     </l>
     <pb facs="2" n="2" xml:id="B20750-002-a"/>
     <l xml:id="B20750-e10150">
      <w lemma="and" pos="cc" xml:id="B20750-002-a-0010">And</w>
      <w lemma="if" pos="cs" xml:id="B20750-002-a-0020">if</w>
      <w lemma="it" pos="pn" xml:id="B20750-002-a-0030">it</w>
      <w lemma="be" pos="vvb" xml:id="B20750-002-a-0040">be</w>
      <w lemma="thy" pos="po" xml:id="B20750-002-a-0050">thy</w>
      <w lemma="fate" pos="n1" xml:id="B20750-002-a-0060">Fate</w>
      <w lemma="again" pos="av" xml:id="B20750-002-a-0070">again</w>
      <w lemma="to" pos="acp" xml:id="B20750-002-a-0080">to</w>
      <w lemma="marry" pos="vvi" reg="mary" xml:id="B20750-002-a-0090">Marry</w>
      <pc xml:id="B20750-002-a-0100">,</pc>
     </l>
     <l xml:id="B20750-e10160">
      <w lemma="and" pos="cc" xml:id="B20750-002-a-0110">And</w>
      <w lemma="S—y" pos="n1" rend="hi" xml:id="B20750-002-a-0140">S—y</w>
      <pc xml:id="B20750-002-a-0150">—</pc>
      <w lemma="r" pos="nng1" rend="hi-apo-plain" xml:id="B20750-002-a-0170">r's</w>
      <w lemma="daughter" pos="n1" xml:id="B20750-002-a-0180">Daughter</w>
      <w lemma="will" pos="vmb" xml:id="B20750-002-a-0190">will</w>
      <w lemma="thy" pos="po" xml:id="B20750-002-a-0200">thy</w>
      <w lemma="year" pos="n1" xml:id="B20750-002-a-0210">Year</w>
      <w lemma="out" pos="av" xml:id="B20750-002-a-0220">out</w>
      <w lemma="tarry" pos="vvi" xml:id="B20750-002-a-0230">tarry</w>
      <pc xml:id="B20750-002-a-0240">,</pc>
     </l>
     <l xml:id="B20750-e10170">
      <w lemma="may" pos="vm2" reg="mayst" xml:id="B20750-002-a-0250">May'st</w>
      <w lemma="thou" pos="pns" xml:id="B20750-002-a-0260">thou</w>
      <w lemma="use" pos="vvi" xml:id="B20750-002-a-0270">use</w>
      <w lemma="she" pos="pno" xml:id="B20750-002-a-0280">her</w>
      <w lemma="as" pos="acp" xml:id="B20750-002-a-0290">as</w>
      <w lemma="mohun" pos="nn1" rend="hi" xml:id="B20750-002-a-0300">Mohun</w>
      <w lemma="his" pos="po" xml:id="B20750-002-a-0310">his</w>
      <w lemma="tender" pos="j" xml:id="B20750-002-a-0320">tender</w>
      <w lemma="wife" pos="n1" xml:id="B20750-002-a-0330">Wife</w>
      <pc xml:id="B20750-002-a-0340">,</pc>
     </l>
     <l xml:id="B20750-e10180">
      <w lemma="and" pos="cc" xml:id="B20750-002-a-0350">And</w>
      <w lemma="may" pos="vmb" xml:id="B20750-002-a-0360">may</w>
      <w lemma="she" pos="pns" xml:id="B20750-002-a-0370">she</w>
      <w lemma="lead" pos="vvi" reg="led" xml:id="B20750-002-a-0380">lead</w>
      <w lemma="his" pos="po" xml:id="B20750-002-a-0390">his</w>
      <w lemma="virtuous" pos="j" xml:id="B20750-002-a-0400">virtuous</w>
      <w lemma="lady" pos="ng1" xml:id="B20750-002-a-0410">Lady's</w>
      <w lemma="life" pos="n1" xml:id="B20750-002-a-0420">Life</w>
      <pc unit="sentence" xml:id="B20750-002-a-0430">.</pc>
     </l>
     <l xml:id="B20750-e10190">
      <w lemma="to" pos="acp" xml:id="B20750-002-a-0440">To</w>
      <w lemma="sum" pos="n1" xml:id="B20750-002-a-0450">sum</w>
      <w lemma="up" pos="acp" xml:id="B20750-002-a-0460">up</w>
      <w lemma="all" pos="d" xml:id="B20750-002-a-0470">all</w>
      <pc xml:id="B20750-002-a-0480">;</pc>
      <w lemma="Devonshire" pos="nng1" rend="hi-apo-plain" xml:id="B20750-002-a-0500">Devonshire's</w>
      <w lemma="chastity" pos="n1" xml:id="B20750-002-a-0510">Chastity</w>
      <pc xml:id="B20750-002-a-0520">,</pc>
     </l>
     <l xml:id="B20750-e10200">
      <w lemma="Bolton" pos="nng1" rend="hi-apo-plain" xml:id="B20750-002-a-0540">Bolton's</w>
      <w lemma="merit" pos="n1" xml:id="B20750-002-a-0550">Merit</w>
      <pc xml:id="B20750-002-a-0560">,</pc>
      <w lemma="Godolphin" pos="nng1" rend="hi-apo-plain" xml:id="B20750-002-a-0580">Godolphin's</w>
      <w lemma="probity" pos="n1" xml:id="B20750-002-a-0590">Probity</w>
      <pc xml:id="B20750-002-a-0600">,</pc>
     </l>
     <l xml:id="B20750-e10210">
      <w lemma="halifax" pos="nn1" rend="hi" xml:id="B20750-002-a-0610">Halifax</w>
      <w lemma="his" pos="po" xml:id="B20750-002-a-0620">his</w>
      <w lemma="modesty" pos="n1" xml:id="B20750-002-a-0630">Modesty</w>
      <pc xml:id="B20750-002-a-0640">,</pc>
      <w lemma="Essex" pos="nng1" rend="hi-apo-plain" xml:id="B20750-002-a-0660">Essex's</w>
      <w lemma="sense" pos="n1" xml:id="B20750-002-a-0670">Sense</w>
      <pc xml:id="B20750-002-a-0680">,</pc>
     </l>
     <l xml:id="B20750-e10220">
      <w lemma="Mountague" pos="nng1" rend="hi-apo-plain" xml:id="B20750-002-a-0700">Mountague's</w>
      <w lemma="management" pos="n1" xml:id="B20750-002-a-0710">Managment</w>
      <pc xml:id="B20750-002-a-0720">,</pc>
      <w lemma="Culpepper" pos="nng1" rend="hi-apo-plain" xml:id="B20750-002-a-0740">Culpepper's</w>
      <w lemma="penny" pos="n2" xml:id="B20750-002-a-0750">Pence</w>
      <pc xml:id="B20750-002-a-0760">,</pc>
     </l>
     <l xml:id="B20750-e10230">
      <w lemma="Tenison" pos="nng1" rend="hi-apo-plain" xml:id="B20750-002-a-0780">Tenison's</w>
      <w lemma="learning" pos="n1" xml:id="B20750-002-a-0790">Learning</w>
      <pc xml:id="B20750-002-a-0800">,</pc>
      <w lemma="and" pos="cc" xml:id="B20750-002-a-0810">and</w>
      <w lemma="Southampton" pos="nng1" rend="hi-apo-plain" xml:id="B20750-002-a-0830">Southampton's</w>
      <w lemma="wit" pos="n1" xml:id="B20750-002-a-0840">Wit</w>
      <pc xml:id="B20750-002-a-0850">,</pc>
     </l>
     <l xml:id="B20750-e10240">
      <w lemma="will" pos="vmb" xml:id="B20750-002-a-0860">Will</w>
      <w lemma="make" pos="vvi" xml:id="B20750-002-a-0870">make</w>
      <w lemma="thou" pos="png" xml:id="B20750-002-a-0880">thee</w>
      <w lemma="for" pos="acp" xml:id="B20750-002-a-0890">for</w>
      <w lemma="a" pos="d" xml:id="B20750-002-a-0900">an</w>
      <w lemma="able" pos="j" xml:id="B20750-002-a-0910">able</w>
      <w lemma="statesman" pos="n1" xml:id="B20750-002-a-0920">States-man</w>
      <w lemma="fit" pos="j" xml:id="B20750-002-a-0930">fit</w>
      <pc unit="sentence" xml:id="B20750-002-a-0940">.</pc>
     </l>
    </lg>
    <trailer xml:id="B20750-e10250">
     <w lemma="n/a" pos="fla" xml:id="B20750-002-a-0950">FINIS</w>
     <pc unit="sentence" xml:id="B20750-002-a-0960">.</pc>
    </trailer>
   </div>
  </body>
 </text>
</TEI>
